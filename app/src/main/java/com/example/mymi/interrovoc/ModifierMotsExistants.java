package com.example.mymi.interrovoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class ModifierMotsExistants extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifier_mots_existants);
    }

    public void terminerModif(View view) {
        //enregistrer les modifs

        //retourner à l'écran d'accueil
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture Modifier Mots Existants");
    }
}
