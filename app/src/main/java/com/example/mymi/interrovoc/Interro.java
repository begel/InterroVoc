package com.example.mymi.interrovoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Interro extends AppCompatActivity {
    ListView mListView;
    ClassBDD bdd;
    ArrayList<Mot> motsDepart;
    Random random;
    Mot motRandom;
    int nbMotsDepart;
    HashMap<Integer,ArrayList<String>> traductions;
    TextView motATraduire;
    ArrayList<StringEtID> languesArrivee;
    List<EditTextLabel> languesEditText;
    boolean hasPassed;
    boolean hasFailed;
    Button boutonPasse;
    //total - bons - faux puis bons - passés
    int score[]= {0,0,0,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interro);
        bdd = new ClassBDD(this);
        bdd.open();

        Intent intent = getIntent();
        languesArrivee = intent.getParcelableArrayListExtra("languesArrivee");
        Log.d(Constants.LOG, "[Para] Langues d'arrivée au nombre de " + languesArrivee.size());
        ArrayList<StringEtID> themes = intent.getParcelableArrayListExtra("themes");
        Log.d(Constants.LOG, "[Para] Themes au nombre de " + themes.size());
        StringEtID langueDepart = intent.getParcelableExtra("langueDepart");
        Log.d(Constants.LOG, "[Para] Langue de départ pour l'interro " + langueDepart);

        // Initialisation
        random = new Random();
        hasPassed = false;
        hasFailed = false;
        motsDepart = new ArrayList<>();
        motATraduire = (TextView) findViewById(R.id.textView10);

        mListView = (ListView) findViewById(R.id.listView3);
        boutonPasse = (Button) findViewById(R.id.button6);
        boutonPasse.setText(R.string.je_passe);
        languesEditText = new ArrayList<>();
        for (StringEtID langue : languesArrivee) {
            languesEditText.add(new EditTextLabel(langue));
        }
        creerListViewEditText();
        trouverMotsDeparts(themes, langueDepart);
        choisisMot();
        Log.d(Constants.LOG, "[Act] Ouverture Interro");
    }

    public void startAccueil(View view) {
        bdd.close();
        Toast.makeText(this,"Bons du 1er coup : "+score[1]+"  Bons après échec : "+score[2]
                +" Passés : "+score[3]+" Total : "+score[0],Toast.LENGTH_SHORT).show();
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture Interro");
    }

    public void verifierTraduction(View view) {
        //L'ordre des traductions dans champsTextes correspond à l'ordre du constructeur de l'adapteur
        // donc dans l'ordre de languesArrivee
        if (hasPassed){
            choisisMot();
            creerListViewEditText();
            boutonPasse.setText(R.string.je_passe);
            Log.d(Constants.LOG,"[Interro] Mot suivant après avoir passé");
        } else {
            EditTextLabelAdapter adapter = (EditTextLabelAdapter) mListView.getAdapter();
            ArrayList<StringEtID> champsTextes = adapter.getChampsTextes();
            int lid;
            boolean toutBon = true;
            for (int i = 0; i < champsTextes.size(); i++) {
                lid = languesArrivee.get(i).getId();
                if (traductions.containsKey(lid)) {
                    if (traductions.get(lid).contains(champsTextes.get(i).getLabel())) {
                        adapter.setChampsTextesColor(i, Constants.COLOR_OK);
                    } else {
                        adapter.setChampsTextesColor(i, Constants.COLOR_NO);
                        toutBon = false;
                    }
                }
            }
            if (toutBon) {
                Toast.makeText(this, "Traductions correctes !", Toast.LENGTH_SHORT).show();
                score[0]++;
                if (hasFailed) {
                    score[2]++;
                } else {
                    score[1]++;
                }
                choisisMot();
                creerListViewEditText();
                Log.d(Constants.LOG, "[Interro] Traduction correcte !");
            } else {
                Toast.makeText(this, "Try again", Toast.LENGTH_SHORT).show();
                mListView.setAdapter(adapter);
                hasFailed = true;
                Log.d(Constants.LOG, "[Interro] Faute dans les traductions");
            }
        }
    }

    public void changerMot(View view) {
        if (hasPassed){
            choisisMot();
            creerListViewEditText();
            boutonPasse.setText(R.string.je_passe);
            Log.d(Constants.LOG,"[Interro] Mot suivant après avoir passé");
        } else {
            EditTextLabelAdapter adapter = (EditTextLabelAdapter)mListView.getAdapter();
            ArrayList<StringEtID> champsTextes = adapter.getChampsTextes();
            int lid;
            for (int i = 0; i < champsTextes.size(); i++) {
                lid = languesArrivee.get(i).getId();
                if (traductions.containsKey(lid)){
                    adapter.setUnChampsTextes(i,Constants.COLOR_NO,traductions.get(lid).get(0));
                } else {
                    adapter.setUnChampsTextes(i, Constants.COLOR_NEUTRE, "");
                }
            }
            mListView.setAdapter(adapter);
            boutonPasse.setText(R.string.suivant);
            score[0]++;
            score[3]++;
            Log.d(Constants.LOG,"[Interro] Mot passé");
            hasPassed = true;
        }
    }

    /**
     * Parmi les mots de départ, en sélectionne un au hasard.
     * Ensuite, on trouve ces traductions associées.
     * Si pour une langue il n'a pas de traduction, on remplis l'EditText avec une valeur par défaut
     */
    //TODO l'affichage des langues pour lesquelles on a pas de traduction marche pas
    public void choisisMot(){
        motRandom = motsDepart.get(random.nextInt(nbMotsDepart));
        motATraduire.setText(motRandom.getMot().toString());
        traductions = bdd.getMotsTraduits(motRandom, languesArrivee);
        TextView tradIndispo = (TextView) findViewById(R.id.textView20);
        String messageIndispo = Constants.INDISPO;
        int j;
        hasFailed = false;
        hasPassed = false;
        for (StringEtID langue: languesArrivee){
            j = langue.getId();
            if (!traductions.containsKey(j)){
                messageIndispo = messageIndispo + langue.getLabel();
            }
        }
        tradIndispo.setText(messageIndispo);
    }

    /**
     * Construit l'adapter pour la ListView de TextFieldLangue
     * Pour chaque langue:
     * - on ajoute un TextFieldLangue à la liste
     * - on peuple languesArriveeMap et languesArriveeID
     * On construit l'adapter avec la liste et on l'attribue à mListView
     */
    private void creerListViewEditText(){
        EditTextLabelAdapter adapter = new EditTextLabelAdapter(this, languesEditText);
        mListView.setAdapter(adapter);
        Log.d(Constants.LOG,"[Interro] ListView mise à jour");
    }

    /**
     * Selon les thèmes et la langue de départ va trouver les mots de départ possibles.
     * @param langueDepart
     * @param themes
     * Pour chaque thème fait la requête sql
     */
    private void trouverMotsDeparts(ArrayList<StringEtID> themes, StringEtID langueDepart){
        List<Mot> requete;
        for (StringEtID theme : themes){
            requete = bdd.getMotsWithTidLid(theme.getId(), langueDepart.getId());
            if (requete != null){
                motsDepart.addAll(requete);
                Log.d(Constants.LOG, "[Info] Nombre de mots initiaux pour le theme " + theme.getLabel() + " " +requete.size());
            }
            else {
                Log.d(Constants.LOG,"[Error] Aucun mot trouvé pour le theme " + theme.getLabel());
            }
        nbMotsDepart = motsDepart.size();
        Log.d(Constants.LOG,"[Info] Nombre de mots initiaux " + nbMotsDepart);
        }
    }
}
