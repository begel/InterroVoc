package com.example.mymi.interrovoc;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class pour représenter les langues et les thèmes.
 * @author Myriam Bégel
 */
public class StringEtID implements Parcelable {
    private int id;
    private String label;

    public StringEtID() {
    }

    public StringEtID(int id, String label) {
        this.id = id;
        this.label = label;
    }

    protected StringEtID(Parcel in) {
        id = in.readInt();
        label = in.readString();
    }

    public static final Parcelable.Creator<StringEtID> CREATOR = new Parcelable.Creator<StringEtID>() {
        @Override
        public StringEtID createFromParcel(Parcel in) {
            return new StringEtID(in);
        }

        @Override
        public StringEtID[] newArray(int size) {
            return new StringEtID[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public Parcelable.Creator<StringEtID> getCreator() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(label);
    }

    @Override
    public String toString(){
        return label;
    }
}
