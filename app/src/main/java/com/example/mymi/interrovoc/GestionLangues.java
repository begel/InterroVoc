package com.example.mymi.interrovoc;

import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

public class GestionLangues extends AppCompatActivity implements PopUpConfirmation.PopUpConfirmationListener {
    ClassBDD bdd;
    Spinner spinner;
    List<StringEtID> langues;
    StringEtID langueChoisie;
    EditText champNewLangue;
    EditText champModifLangue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestion_langues);
        bdd = new ClassBDD(this);
        bdd.open();
        spinner = (Spinner) findViewById(R.id.spinner3);
        spinnerUpdate();
        champNewLangue = (EditText) findViewById(R.id.editText2);
        champModifLangue = (EditText) findViewById(R.id.editText4);
        Log.d(Constants.LOG, "[Act] Ouverture GestionLangues");
    }

    private void spinnerUpdate(){
        langues = bdd.getLangues();
        Log.d(Constants.LOG,"[Langues] langues "+langues);
        if (langues.size()>0){
            ArrayAdapter<StringEtID> tadapterSpinner = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, langues);
            tadapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(tadapterSpinner);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    langueChoisie = (StringEtID) parent.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    public void nouvelleLangue(View view){
        if (!champNewLangue.getText().toString().equals("")){
            bdd.insertLangue(champNewLangue.getText().toString());
            spinnerUpdate();
            champNewLangue.setText("");
            Toast.makeText(this,"Langue ajoutée !",Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,"Ecris quelque chose avant d'enregister",Toast.LENGTH_SHORT).show();
        }
    }

    public void updateLangue(View view){
        if (langues.size()>0 && !champModifLangue.getText().toString().equals("")) {
            bdd.updateLangue(langueChoisie.getId(), champModifLangue.getText().toString());
            spinnerUpdate();
            champModifLangue.setText("");
            Log.d(Constants.LOG, "[Langue] langue updated");
            Toast.makeText(this,"Langue mise à jour !",Toast.LENGTH_SHORT);
        } else {
            Toast.makeText(this,"Rien à modifier",Toast.LENGTH_SHORT).show();
        }
    }

    public void backAccueil(View view){
        bdd.close();
        finish();
    }

    public void supprLangue(View view){
        if (langues.size()>0) {
            DialogFragment dialog = new PopUpConfirmation();
            dialog.show(getFragmentManager(), "PopUpConfirmation");
        } else {
            Toast.makeText(this,"Pas de langue à supprimer",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Fonction de gestion du bouton OUI de la popup
     * @param dialog
     * Supprime le thème de la BDD et retourne à la page d'accueil
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        bdd.removeLangue(langueChoisie);
        spinnerUpdate();
        dialog.dismiss();
        Toast.makeText(this,"Langue supprimée !",Toast.LENGTH_SHORT).show();
        Log.d(Constants.LOG,"[Langue] Langue supprimée");
    }

    /**
     * Fonction de gestion du bouton NON de la popup
     * @param dialog
     * Quitte la popup sans rien faire d'autre
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
    }
}
