package com.example.mymi.interrovoc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Liaison entre Mot et SQLite
 * @author Myriam Bégel
 */
public class ClassBDD{
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "dico.db";

    //Table pour les mots
    private static final String TABLE_MOTS = "table_mots";
    private static final String COL_MID = "mID";
    private static final int NUM_COL_MID = 0;
    private static final String COL_MOT = "mot";
    private static final int NUM_COL_MOT = 1;
    private static final String COL_MLANGUEID = "mlangueID";
    private static final int NUM_COL_MLANGUEID = 2;
    private static final String COL_MSEMANTIQUEID = "msemantiqueID";
    private static final int NUM_COL_MSEMANTIQUEID = 3;
    private static final String COL_MTHEMEID = "mthemeID";
    private static final int NUM_COL_MTHEMEID = 4;

    //Table pour les langues
    private static final String TABLE_LANGUE = "table_langues";
    private static final String COL_LID = "lID";
    private static final int NUM_COL_LID = 0;
    private static final String COL_LANGUE = "langue";
    private static final int NUM_COL_LANGUE = 1;

    //Table pour les themes
    private static final String TABLE_THEME = "table_themes";
    private static final String COL_TID = "tID";
    private static final int NUM_COL_TID = 0;
    private static final String COL_THEME = "theme";
    private static final int NUM_COL_THEME = 1;

    //Table pour les semantiques
    private static final String TABLE_SEM = "table_semantique";
    private static final String COL_SID = "sID";
    private static final int NUM_COL_SID = 0;
    private static final String COL_COMPT = "compteur";
    private static final int NUM_COL_COMPT = 1;

    private SQLiteDatabase bdd;

    private MaBaseSQLite maBaseSQLite;

    public ClassBDD(Context context){
        //On crée la BDD et sa table
        maBaseSQLite = new MaBaseSQLite(context, NOM_BDD, null, VERSION_BDD);
    }

    /**
     * * Open the database* *
     *
     * @throws SQLiteException
     */
    public void open() throws SQLiteException {
        try {
            bdd = maBaseSQLite.getWritableDatabase();
            if (BuildConfig.DEBUG) {
                Log.d(Constants.LOG, "[SQL] Base ouverte en écriture");
            }
        } catch (SQLiteException ex) {
            bdd = maBaseSQLite.getReadableDatabase();
            if (BuildConfig.DEBUG) {
                Log.d(Constants.LOG, "[SQL] Base ouverte en lecture seule");
            }
        }
    }

    /** *Close Database */
    public void close() {
        bdd.close();
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Base fermée");
        }
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    /**
     * Insère un mot dans la BDD.
     * @param mot
     * @param mlangueID
     * @param mthemeID
     * @param msemantiqueID
     * @return résultat de la bdd
     * Crée un ContentValues (fonctionne comme une HashMap)
     * on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
     * On finit par la requête sql
     */
    public long insertMot(String mot, int mlangueID, int mthemeID, int msemantiqueID){
        ContentValues values = new ContentValues();
        values.put(COL_MOT, mot);
        values.put(COL_MLANGUEID, mlangueID);
        values.put(COL_MTHEMEID, mthemeID);
        values.put(COL_MSEMANTIQUEID, msemantiqueID);
        long res =  bdd.insert(TABLE_MOTS, null, values);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Insertion mot " + mot + " succès: " + (res>=0));
        }
        return res;
    }

    /**
     * Insère une langue dans la BDD
     * @param langue
     * @return résultat de la bdd
     * Crée un ContentValues (fonctionne comme une HashMap)
     * on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
     * On finit par la requête sql
     */
    public long insertLangue(String langue){
        ContentValues values = new ContentValues();
        values.put(COL_LANGUE, langue);
        long res =  bdd.insert(TABLE_LANGUE, null, values);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Insertion langue " + langue + " succès : " + (res>=0));
        }
        return res;
    }

    /**
     * Insère un thème dans la BDD
     * @param theme
     * @return résultat de la bdd
     * Crée un ContentValues (fonctionne comme une HashMap)
     * on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
     * On finit par la requête sql
     */
    public long insertTheme(String theme){
        ContentValues values = new ContentValues();
        values.put(COL_THEME, theme);
        long res =  bdd.insert(TABLE_THEME, null, values);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Insertion theme " + theme + " succès : " + (res>=0));
        }
        return res;
    }

    /**
     * Met à jour un mot dans la BDD
     * @param mid
     * @param mot
     * @return résultat de la BDD
     * Fonctionne un peu comme insertion
     */
    public int updateMot(int mid, Mot mot){
        ContentValues values = new ContentValues();
        values.put(COL_MOT, mot.getMot());
        values.put(COL_MLANGUEID, mot.getMlangueID());
        values.put(COL_MSEMANTIQUEID, mot.getMsemantiqueID());
        values.put(COL_MTHEMEID, mot.getMthemeID());
        int res =  bdd.update(TABLE_MOTS, values, COL_MID + " = " + mid, null);
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Update mot" + mot.getMot() + "succès : " + (res>=0));
        }
        return res;
    }

    public int updateLangue(int lid, String langue){
        ContentValues values = new ContentValues();
        values.put(COL_LID,lid);
        values.put(COL_LANGUE,langue);
        int res =  bdd.update(TABLE_LANGUE, values, COL_LID + " = " + lid, null);
        Log.d(Constants.LOG, "[SQL] Update langue" + lid + "succès : " + (res>=0));
        return res;
    }

    /**
     * Donne le SID suivant pour l'ajout de mot.
     * @return le prochain sid à utiliser
     * Les SID permettent de regrouper entre eux les mots de plusieurs langues qui sont ajoutés ensemble
     * La table ne contient qu'une donnée qu'on incrémente à chaque fois que nécessaire
     */
    public int prochaineSemantique(){
        Cursor c = bdd.query(TABLE_SEM, new String[]{COL_SID,COL_COMPT},null, null, null, null, null);
        c.moveToFirst();
        int compt = c.getInt(NUM_COL_COMPT);
        int sid = c.getInt(NUM_COL_SID);
        ContentValues values = new ContentValues();
        values.put(COL_COMPT, compt+1);
        bdd.update(TABLE_SEM, values, COL_SID + " = " + sid, null);
        return compt;
    }

    /**
     * Suppression d'un mot de la BDD grâce à l'ID
     * @param mid
     * @return
     */
    public int removeMotWithID(int mid){
        return bdd.delete(TABLE_MOTS, COL_MID + " = " + mid, null);
    }


    /**
     * Retire le thème et tous les mots qui ont ce thème là
     * @param theme
     * @return
     */
    public int removeTheme(StringEtID theme){
        int tid = theme.getId();
        bdd.delete(TABLE_MOTS, COL_MTHEMEID + " = " + tid, null);
        Log.d(Constants.LOG, "[SQL] Remove theme " + theme.getLabel());
        return bdd.delete(TABLE_THEME, COL_TID + " = " + tid, null);
    }

    public int removeLangue(StringEtID langue){
        int lid = langue.getId();
        bdd.delete(TABLE_MOTS, COL_MLANGUEID + " = " + lid, null);
        Log.d(Constants.LOG, "[SQL] Remove langue " + langue.getLabel());
        return bdd.delete(TABLE_LANGUE,COL_LID + " = " + lid, null);
    }

    /**
     * Récupère les mots grâce au thème et la langue
     * @param tid
     * @param lid
     * @return l'ensemble des mots qui ont ce thème là
     * Utile pour commencer une interro.
     * On fait la recherche des mots avec le tid et le lid donné.
     */
    public List<Mot> getMotsWithTidLid(int tid, int lid){
        Cursor c = bdd.query(TABLE_MOTS,
                new String[]{COL_MID, COL_MOT, COL_MLANGUEID, COL_MSEMANTIQUEID, COL_MTHEMEID},
                COL_MTHEMEID + " = " + tid + " AND " + COL_MLANGUEID + " = " + lid,
                null, null, null, null);
        return cursorToMots(c);
    }

    /**
     * Pour obtenir toutes les langues enregistrées.
     * @return l'ensemble des langues de la bdd
     */
    public ArrayList<StringEtID> getLangues(){
        Cursor c = bdd.query(TABLE_LANGUE, new String[]{COL_LID, COL_LANGUE}, null, null, null, null, null);
        return cursorToStringEtID(c);
    }

    public HashMap<Integer,String> getLanguesMap(){
        Cursor c = bdd.query(TABLE_LANGUE, new String[]{COL_LID, COL_LANGUE}, null, null, null, null, null);
        return cursorToMap(c);
    }

    /**
     * Pour obtenir l'ensemble des thèmes
     * @return l'ensemble des thèmes de la bdd
     */
    public List<StringEtID> getThemes(){
        Cursor c = bdd.query(TABLE_THEME, new String[]{COL_TID, COL_THEME}, null, null, null, null, null);
        return cursorToStringEtID(c);
    }

    /**
     * Etant donné un mot, retourne toutes ses traductions
     * @param mot
     * @param langues
     * @return
     * D'abord, récupère toutes les instances du mot, au cas où il aurait été ajouté en plusieurs fois.
     * Pour chacun des sid, récupère les mots dans les autres langues qui correspondent.
     * Parmi les réponses, celles qui sont dans une langue voulue sont ajoutées.
     */
    public HashMap<Integer,ArrayList<String>> getMotsTraduits(Mot mot, ArrayList<StringEtID> langues){
        HashMap<Integer,ArrayList<String>> motsTraduits = new HashMap<>();
        ArrayList<Integer> languesID = new ArrayList<>();
        for (StringEtID langue : langues){
            languesID.add(langue.getId());
        }
        Cursor c1 = bdd.query(TABLE_MOTS, new String[]{COL_MLANGUEID,COL_MSEMANTIQUEID}, COL_MOT + " LIKE '" + mot.getMot() + "' " ,null,null,null,null);
        c1.moveToFirst();
        int sid;
        int lid;
        ArrayList<String> listeActuelle;
        Log.d(Constants.LOG,"[SQL] Il y a "+c1.getCount()+" mots traduits pour "+mot.getMot().toString());
        Cursor c;
        for (int i=1; i<=c1.getCount(); i++){
            if (c1.getInt(0)==mot.getMlangueID()){
                sid = c1.getInt(1);
                c = bdd.query(TABLE_MOTS, new String[]{COL_MOT,COL_MLANGUEID}, COL_MSEMANTIQUEID + " = " + sid,null,null,null,null);
                c.moveToFirst();
                for (int j=1; j<=c.getCount(); j++){
                    lid = c.getInt(1);
                    if (languesID.contains(lid)){
                        listeActuelle = motsTraduits.get(lid);
                        //motsTraduits.remove(lid);
                        if (listeActuelle==null){
                            listeActuelle = new ArrayList<>();
                        }
                        listeActuelle.add(c.getString(0));
                        motsTraduits.put(lid, listeActuelle);
                    }
                    c.moveToNext();
                }
            }
            c1.moveToNext();
        }
        return motsTraduits;
    }

    public ArrayList<Mot> getMotsTraduits(String mot){
        ArrayList<Mot> motsTraduits = new ArrayList<>();
        Cursor c1 = bdd.query(TABLE_MOTS, new String[]{COL_MLANGUEID,COL_MSEMANTIQUEID},
                COL_MOT + " LIKE '" + mot + "' " ,null,null,null,null);
        c1.moveToFirst();
        int sid;
        Log.d(Constants.LOG,"[SQL] Il y a "+c1.getCount()+" mots traduits pour "+mot);
        Cursor c;
        while (!c1.isAfterLast()){
            sid = c1.getInt(1);
            c = bdd.query(TABLE_MOTS, new String[]{COL_MID, COL_MOT, COL_MLANGUEID, COL_MTHEMEID, COL_MSEMANTIQUEID},
                    COL_MSEMANTIQUEID + " = " + sid, null, null, null, null);
            motsTraduits.addAll(cursorToMots(c));
            c1.moveToNext();
        }
        return motsTraduits;
    }

    public List<Mot> getMots(){
        Cursor c = bdd.query(TABLE_MOTS, new String[]{COL_MID,COL_MOT,COL_MLANGUEID,COL_MTHEMEID,COL_MSEMANTIQUEID},
                null,null,null,null,null);
        return cursorToMots(c);
    }

    /**
     * Avec un cursor, établi une liste d'objets MOT
     * @param c
     * @return les mots sous forme de List<Mot>
     * S'il n'y a aucun mot, on commence par retourner null.
     * Sinon, pour chaque élément, on crée le mot et on l'ajoute à la liste.
     */
    private List<Mot> cursorToMots(Cursor c){
        if (c.getCount() == 0)
            return null;
        Mot mot;
        int nbReponses = c.getCount();
        List reponses = new ArrayList() ;
        c.moveToFirst();
        for (int i=0; i<nbReponses; i++) {
            mot = cursorToMot(c,i);
            reponses.add(mot);
            c.moveToNext();
        }
        c.close();
        return reponses;
    }

    /**
     * Récupère les éléments du cursor à la bonne position pour construire l'objet mot.
     * @param c
     * @param position
     * @return un objet Mot
     */
    private Mot cursorToMot(Cursor c, int position){
        c.moveToPosition(position);
        int mid = c.getInt(NUM_COL_MID);
        String stringMot = c.getString(NUM_COL_MOT);
        int mlangueID = c.getInt(NUM_COL_MLANGUEID);
        int msemantiqueID = c.getInt(NUM_COL_MSEMANTIQUEID);
        int mthemeID = c.getInt(NUM_COL_MTHEMEID);
        return new Mot(mid, stringMot, mlangueID, msemantiqueID, mthemeID);
    }

    /**
     * Avec un cursor, établi une liste d'objets LANGUE
     * @param c
     * @return les objets du cursor sous forme de List<StringEtID>
     * S'il n'y a pas d'objet : retourne null
     * Sinon, pour chaque élément, on l'ajoute à la liste
     */
    private ArrayList<StringEtID> cursorToStringEtID(Cursor c){
        ArrayList reponses = new ArrayList() ;
        if (c.getCount() == 0)
            return reponses;
        c.moveToFirst();
        StringEtID objet;
        while (!c.isAfterLast()) {
            objet = new StringEtID(c.getInt(0),c.getString(1));
            reponses.add(objet);
            c.moveToNext();
        }
        c.close();
        return reponses;
    }

    private HashMap<Integer,String> cursorToMap(Cursor c){
        HashMap<Integer,String> reponses = new HashMap<>() ;
        if (c.getCount() == 0)
            return reponses;
        int nbReponses = c.getCount();
        c.moveToFirst();
        while (!c.isAfterLast()) {
            reponses.put(c.getInt(0),c.getString(1));
            c.moveToNext();
        }
        c.close();
        return reponses;
    }
}
