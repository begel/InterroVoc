package com.example.mymi.interrovoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ParametresInterro extends AppCompatActivity {
    ClassBDD bdd;
    ListView lListView;
    ListView tListView;
    Spinner spinner;
    ArrayList<StringEtID> languesCochees;
    ArrayList<StringEtID> themesCochees;
    StringEtID langueDepart;
    List<StringEtID> langues;
    List<StringEtID> themes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres_interro);
        bdd = new ClassBDD(this);
        bdd.open();
        langues = bdd.getLangues();
        themes = bdd.getThemes();
        bdd.close();
        if (langues.size()==0 || themes.size()==0){
            Toast.makeText(this, "Vérifie que tu as ajouté des langues et des thèmes avant de commencer une interro",
                    Toast.LENGTH_LONG).show();
            finish();
        }
        //ListView de thèmes

        tListView = (ListView) findViewById(R.id.listView);
        CheckBoxTextAdapter tadapter = new CheckBoxTextAdapter(this, themes);
        tListView.setAdapter(tadapter);
        themesCochees = new ArrayList<>();

        //spinner
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<StringEtID> ladapterSpinner = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, langues);
        ladapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(ladapterSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                langueDepart = langues.get(position);
                Log.d(Constants.LOG,"[Spin] Langue suivante sélectionnée "+langueDepart.getLabel());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //ListView de langues checkbox
        lListView = (ListView) findViewById(R.id.listView2);
        CheckBoxTextAdapter ladapter = new CheckBoxTextAdapter(this, langues);
        lListView.setAdapter(ladapter);
        languesCochees = new ArrayList<>();
        Log.d(Constants.LOG,"[Act] Ouverture ParametresInterro");
    }

    public void enregistreParametresLangues(){
        CheckBoxTextAdapter adapter = (CheckBoxTextAdapter) lListView.getAdapter();
        boolean[] lChecked = adapter.getMChecked();
        for (int i=0; i<lChecked.length;i++){
            if (lChecked[i]){
                languesCochees.add(langues.get(i));
            }
        }
    }

    public void enregistreParametresThemes(){
        CheckBoxTextAdapter adapter = (CheckBoxTextAdapter) tListView.getAdapter();
        boolean[] tChecked = adapter.getMChecked();
        for (int i=0; i<tChecked.length;i++){
            if (tChecked[i]){
                themesCochees.add(themes.get(i));
            }
        }
    }

    /**
     * Quand on valide on passe à Interro avec les paramètres enregistrés
     * @param view
     */
    public void commencerInterro(View view) {
        enregistreParametresLangues();
        enregistreParametresThemes();
        Intent intent = new Intent(this,Interro.class);
        intent.putParcelableArrayListExtra("languesArrivee", languesCochees);
        intent.putParcelableArrayListExtra("themes", themesCochees);
        intent.putExtra("langueDepart", langueDepart);
        Log.d(Constants.LOG, "[Para] Langue de départ pour l'interro " + langueDepart.getLabel());
        startActivity(intent);
        Log.d(Constants.LOG, "[Act] Fermeture ParamètresInterro");
        finish();
    }
}
