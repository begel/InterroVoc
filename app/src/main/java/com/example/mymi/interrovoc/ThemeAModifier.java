package com.example.mymi.interrovoc;

import android.app.DialogFragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ThemeAModifier extends AppCompatActivity implements PopUpConfirmation.PopUpConfirmationListener {
    ClassBDD bdd;
    Spinner spinner;
    StringEtID themeChoisis;
    ArrayList<StringEtID> languesChoisis;
    ListView mListView;
    List<StringEtID> langues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme_amodifier);
        languesChoisis = new ArrayList<>();
        bdd = new ClassBDD(this);
        bdd.open();
        langues = bdd.getLangues();
        List<StringEtID> themes = bdd.getThemes();
        if (langues.size()==0 || themes.size()==0){
            Toast.makeText(this, "Vérifie que tu as ajouté des langues et des thèmes avant de modifier un thème",
                    Toast.LENGTH_LONG).show();
            bdd.close();
            finish();
        }

        //spinner
        spinnerInitial(themes);

        //listview de checkbox de langues
        mListView = (ListView) findViewById(R.id.listView6);
        CheckBoxTextAdapter adapter = new CheckBoxTextAdapter(this, langues);
        mListView.setAdapter(adapter);
        Log.d(Constants.LOG, "[Act] Overture ThemeAModifier");
    }

    /**
     * Met en place le spinner de choix des thèmes.
     * @param themes
     * Récupère la vue, créer un adapter avec la liste des themes, dit que c'est un dropdown,
     * pour l'élément sélectionné, l'assigne à themeChoisis
     */
    private void spinnerInitial(List<StringEtID> themes){
        spinner = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<StringEtID> tadapterSpinner = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, themes);
        tadapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(tadapterSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                themeChoisis = (StringEtID) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void startModifMots(View view) {
        //passer les paramètres
        enregistreParametres();
        //aller à la page suivante
        bdd.close();
        Intent intent = new Intent(this,ModifierMotsExistants.class);
        startActivity(intent);
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture ThemeAModifier");
    }

    public void startNouveauMot(View view) {
        enregistreParametres();
        bdd.close();
        Intent intent = new Intent(this,NouveauMot.class);
        intent.putExtra("theme",themeChoisis);
        intent.putParcelableArrayListExtra("langues", languesChoisis);
        startActivity(intent);
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture ThemeAModifier");
    }

    /**
     * Ouvre la PopUp qui demande confirmation pour supprimer un thème
     * @param view
     */
    public void supprTheme(View view) {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new PopUpConfirmation();
        dialog.show(getFragmentManager(), "PopUpConfirmation");
    }

    /**
     * Récupère les langues cochées dans l'adapter.
     */
    public void enregistreParametres(){
        CheckBoxTextAdapter adapter = (CheckBoxTextAdapter) mListView.getAdapter();
        boolean[] mChecked = adapter.getMChecked();
        for (int i=0; i<mChecked.length;i++){
            if (mChecked[i]){
                languesChoisis.add(langues.get(i));
            }
        }
    }

    /**
     * Fonction de gestion du bouton OUI de la popup
     * @param dialog
     * Supprime le thème de la BDD et retourne à la page d'accueil
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        bdd.removeTheme(themeChoisis);
        bdd.close();
        dialog.dismiss();
        finish();
        Log.d(Constants.LOG,"[Act] Fermeture ThemeAModifier");
    }

    /**
     * Fonction de gestion du bouton NON de la popup
     * @param dialog
     * Quitte la popup sans rien faire d'autre
     */
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
    }
}
