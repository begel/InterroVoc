package com.example.mymi.interrovoc;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class NouveauTheme extends AppCompatActivity {
    ClassBDD bdd;
    EditText champTheme;
    ListView mListView;
    ArrayList<StringEtID> languesCochees;
    ArrayList<StringEtID> langues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouveau_theme);
        bdd = new ClassBDD(this);
        champTheme = (EditText) findViewById(R.id.editText3);
        mListView = (ListView) findViewById(R.id.listView4);
        bdd.open();
        langues = bdd.getLangues();

        if (langues.size()==0){
            Toast.makeText(this,"Ajoute des langues avant d'ajouter des thèmes",Toast.LENGTH_LONG).show();
            bdd.close();
            finish();
        }
        CheckBoxTextAdapter adapter = new CheckBoxTextAdapter(this, langues);
        mListView.setAdapter(adapter);
        Log.d(Constants.LOG, "[Act] Ouverture NouveauTheme");
    }

    public void enregistreParametres(){
        languesCochees = new ArrayList<>();
        CheckBoxTextAdapter adapter = (CheckBoxTextAdapter) mListView.getAdapter();
        boolean[] mChecked = adapter.getMChecked();
        for (int i=0; i<mChecked.length;i++){
            if (mChecked[i]){
                languesCochees.add(langues.get(i));
            }
        }
    }

    public void creerTheme(View view) {
        //enregistrer nouveau theme
        String theme = champTheme.getText().toString();
        if (theme.equals("")){
            Toast.makeText(this,"Ecris le nom du thème avant de valider",Toast.LENGTH_SHORT).show();
        } else {
            bdd.insertTheme(theme);
            bdd.close();
            Toast.makeText(this, "Thème crée", Toast.LENGTH_SHORT).show();
            enregistreParametres();
            Intent intent;
            if (languesCochees == null || languesCochees.size() == 0) {
                finish();
            } else {
                //aller à la page nouveau mot en transmettant les langues voulues
                intent = new Intent(this, NouveauMot.class);
                intent.putExtra("langues", languesCochees);
                intent.putExtra("theme", theme);
                startActivity(intent);
                finish();
            }
        }
        Log.d(Constants.LOG, "[Act] Fermeture Nouveau Theme");
    }
}
