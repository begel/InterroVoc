package com.example.mymi.interrovoc;

import android.graphics.Color;

/**
 * Created by mymi on 22/03/16.
 */
public interface Constants {
    String LOG = "interrovoc";
    String INDISPO = "Traduction indisponible dans les langues suivantes : ";
    int COLOR_OK = Color.argb(50,0,168,0);
    int COLOR_NO = Color.argb(50,255,0,0);
    int COLOR_NEUTRE = Color.argb(50,0,0,0);
}
