package com.example.mymi.interrovoc;

import android.content.Intent;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class Accueil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        Log.d(Constants.LOG, "[Act] Ouverture Accueil");
        DisplayMetrics out = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(out);
        int hauteur= out.heightPixels/6;
        Button button = (Button) findViewById(R.id.button);
        button.setHeight(hauteur);
        button = (Button) findViewById(R.id.button2);
        button.setHeight(hauteur);
        button = (Button) findViewById(R.id.button17);
        button.setHeight(hauteur);
        button = (Button) findViewById(R.id.button18);
        button.setHeight(hauteur);
        button = (Button) findViewById(R.id.button19);
        button.setHeight(hauteur);
    }

    public void startDico(View view){
        Intent intent = new Intent(this,Dictionnaire.class);
        startActivity(intent);
    }

    public void startInterro(View view){
        Intent intent = new Intent(this,ParametresInterro.class);
        startActivity(intent);
    }

    public void startModifTheme(View view){
        Intent intent = new Intent(this,ThemeAModifier.class);
        startActivity(intent);
    }

    public void startNouvTheme(View view){
        Intent intent = new Intent(this,NouveauTheme.class);
        startActivity(intent);
    }

    public void startNouvLangue(View view){
        Intent intent = new Intent(this,GestionLangues.class);
        startActivity(intent);
    }
}
