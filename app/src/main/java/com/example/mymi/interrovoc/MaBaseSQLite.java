package com.example.mymi.interrovoc;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

/**
 * Base de données pour stocker les mots
 * @author Myriam Bégel
 */
public class MaBaseSQLite extends SQLiteOpenHelper {

    //Table pour les mots
    private static final String TABLE_MOT = "table_mots";
    private static final String COL_MID = "mID";
    private static final String COL_MOT = "mot";
    private static final String COL_MLANGUEID = "mlangueID";
    private static final String COL_MSEMANTIQUEID = "msemantiqueID";
    private static final String COL_MTHEMEID = "mthemeID";

    //Table pour les langues
    private static final String TABLE_LANGUE = "table_langues";
    private static final String COL_LID = "lID";
    private static final String COL_LANGUE = "langue";

    //Table pour les themes
    private static final String TABLE_THEME = "table_themes";
    private static final String COL_TID = "tID";
    private static final String COL_THEME = "theme";

    //Table pour les semantiques
    private static final String TABLE_SEM = "table_semantique";
    private static final String COL_SID = "sID";
    private static final String COL_COMPT = "compteur";

    //String pour la création de chaque table
    private static final String CREATE_LANGUE = "CREATE TABLE IF NOT EXISTS " + TABLE_LANGUE + " ( "
            + COL_LID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_LANGUE + " TEXT NOT NULL);" ;

    private static final String CREATE_THEME = "CREATE TABLE IF NOT EXISTS " + TABLE_THEME + " ( "
            + COL_TID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_THEME + " TEXT NOT NULL);" ;

    private static final String CREATE_SEMANTIQUE = "CREATE TABLE IF NOT EXISTS " + TABLE_SEM + " ( "
            + COL_SID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_COMPT + " INT);" ;

    private static final String CREATE_MOT = "CREATE TABLE IF NOT EXISTS " + TABLE_MOT + " ( "
            + COL_MID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_MOT + " TEXT NOT NULL, "
            + COL_MLANGUEID + " INTEGER, " + COL_MSEMANTIQUEID + " INTEGER, "
            + COL_MTHEMEID + " INTEGER, FOREIGN KEY(" + COL_MLANGUEID + ") REFERENCES "
            + TABLE_LANGUE + "(" + COL_LID + "), FOREIGN KEY(" + COL_MTHEMEID + ") REFERENCES "
            + TABLE_THEME + "(" + COL_TID + ") ); " ;

    public MaBaseSQLite(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on cree la table a partir de la requete ecrite dans la variable CREATE_BDD
        ContentValues values = new ContentValues();
        values.put(COL_COMPT, 0);
        db.beginTransaction();
        db.execSQL(CREATE_LANGUE);
        db.execSQL(CREATE_THEME);
        db.execSQL(CREATE_SEMANTIQUE);
        db.execSQL(CREATE_MOT);
        long res = db.insert(TABLE_SEM, null, values);
        db.setTransactionSuccessful();
        db.endTransaction();
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Tables crées");
            Log.d(Constants.LOG, "[SQL] Table sémantique a une valeur : "+ (res>=0));
        }
    }

    /* TODO décider ce qu'on en fait*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut faire ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
        db.beginTransaction();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOT + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LANGUE + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEM + ";");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_THEME + ";");
        db.setTransactionSuccessful();
        db.endTransaction();
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG, "[SQL] Tables supprimées");
        }
        onCreate(db);
    }
}
