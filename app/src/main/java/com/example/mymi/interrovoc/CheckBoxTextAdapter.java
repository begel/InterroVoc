package com.example.mymi.interrovoc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

/**
* Class Adapter pour les CheckBox à l'intérieur des ListView.
 * @author Myriam Bégel
*/

public class CheckBoxTextAdapter extends ArrayAdapter<StringEtID> {
    private boolean[] mChecked;

    /**
     * Constructeur de beas.
     * @param context
     * @param labels List des labels des checkBox
     */
    public CheckBoxTextAdapter(Context context, List<StringEtID> labels) {
        super(context, 0, labels);
        mChecked = new boolean[labels.size()];
        for (int i=0; i<labels.size();i++){
            mChecked[i] = false;
        }
    }

    /**
     * Attribue à une checkBox son label.
     * Récupère l'élément n position dans la liste des labels donnée en entrée.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.check_box_text,parent, false);
        }
        CheckBoxTextViewHolder viewHolder = (CheckBoxTextViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new CheckBoxTextViewHolder();
            viewHolder.checkBoxH = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(viewHolder);
        }
        //getItem(position) va récupérer l'item [position] de la List<> tweets
        StringEtID objet = getItem(position);
        //il ne reste plus qu'à remplir notre vue
        viewHolder.checkBoxH.setText(objet.getLabel());
        viewHolder.checkBoxH.setTag(Integer.valueOf(position));
        viewHolder.checkBoxH.setChecked(mChecked[position]);
        viewHolder.checkBoxH.setOnCheckedChangeListener(mListener);
        return convertView;
    }

    private class CheckBoxTextViewHolder{
        public CheckBox checkBoxH;
    }

    public boolean[] getMChecked(){
        return mChecked;
    }

    CompoundButton.OnCheckedChangeListener mListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mChecked[(Integer) buttonView.getTag()] = isChecked; // get the tag so we know the row and store the status
            Log.d(Constants.LOG, "[CB] CheckBox cochée ou décochée");
        }
    };
}
