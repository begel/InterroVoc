package com.example.mymi.interrovoc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mymi on 27/03/16.
 */
public class MotAdapter extends ArrayAdapter<Mot> {
    HashMap<Integer,String> langues;

    public MotAdapter(Context context, List<Mot> mots, HashMap<Integer,String> langues) {
        super(context, 0, mots);
        this.langues = langues;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.mot,parent, false);
        }
        MotViewHolder viewHolder = (MotViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new MotViewHolder();
            viewHolder.langue = (TextView) convertView.findViewById(R.id.textView4);
            viewHolder.traduction = (TextView) convertView.findViewById(R.id.textView23);
            convertView.setTag(viewHolder);
        }
        //getItem(position) va récupérer l'item [position] de la List<>
        Mot mot = getItem(position);
        //il ne reste plus qu'à remplir notre vue
        viewHolder.langue.setText(langues.get(mot.getMlangueID()));
        viewHolder.traduction.setText(mot.getMot());
        return convertView;
    }

    private class MotViewHolder {
        public TextView langue;
        public TextView traduction;
    }
}
