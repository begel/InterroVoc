package com.example.mymi.interrovoc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mymi on 27/03/16.
 */
public class EditTextLabelAdapter  extends ArrayAdapter<EditTextLabel> {
    private ArrayList<StringEtID> champsTextes;

    //editTextLabel est la liste des models à afficher
    public EditTextLabelAdapter(Context context, List<EditTextLabel> editTextLabel) {
        super(context, 0, editTextLabel);
        champsTextes = new ArrayList<>();
        for (int i=0; i<editTextLabel.size();i++){
            champsTextes.add(new StringEtID(Constants.COLOR_NEUTRE,""));
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.text_field_langue,parent, false);
        }

        EditTextLabelViewHolder viewHolder = (EditTextLabelViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new EditTextLabelViewHolder();
            viewHolder.labelView = (TextView) convertView.findViewById(R.id.textView2);
            viewHolder.motView = (EditText) convertView.findViewById(R.id.editText5);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List fourni au constructeur
        EditTextLabel editTextLabel = getItem(position);

        //il ne reste plus qu'à remplir notre vue
        viewHolder.labelView.setText(editTextLabel.getStringEtID().getLabel());
        viewHolder.motView.setTag(Integer.valueOf(position));
        viewHolder.motView.setBackgroundColor(champsTextes.get(position).getId());
        viewHolder.motView.setText(champsTextes.get(position).getLabel());
        viewHolder.motView.setOnFocusChangeListener(new TextView.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    TextView txtView = (TextView) view;
                    setChampsTextesText((int) txtView.getTag(), txtView.getText().toString());
                    Log.d(Constants.LOG, "[EdiTxt] Focus : mot enregistré dans champsTextes " + txtView.getText().toString());
                }
            }
        });
        return convertView;
    }

    private class EditTextLabelViewHolder {
        public TextView labelView;
        public EditText motView;
    }

    public ArrayList<StringEtID> getChampsTextes(){
        return champsTextes;
    }

    public void setUnChampsTextes(int position, int color, String txt){
        StringEtID obj = new StringEtID(color,txt);
        champsTextes.set(position,obj);
    }

    public void setChampsTextesColor(int position, int color){
        StringEtID obj = champsTextes.get(position);
        obj.setId(color);
        champsTextes.set(position,obj);
    }

    public void setChampsTextesText(int position, String txt){
        StringEtID obj = champsTextes.get(position);
        obj.setLabel(txt);
        champsTextes.set(position,obj);
    }
}
