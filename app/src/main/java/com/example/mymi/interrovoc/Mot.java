package com.example.mymi.interrovoc;

/**
 * Classe pour définir un mot
 * @author Myriam Bégel
 */
public class Mot {
    private int mid;
    private String mot;
    private int mlangueID;
    private int msemantiqueID;
    private int mthemeID;

    public Mot(){}

    public Mot(int mid, String mot, int mlangueID, int msemantiqueID, int mthemeID){
        this.mid = mid;
        this.mot = mot;
        this.mlangueID = mlangueID;
        this.msemantiqueID = msemantiqueID;
        this.mthemeID = mthemeID;
    }

    public void setMid(int id) {
        this.mid = id;
    }

    public int getMid() {
        return mid;
    }

    public void setMot(String mot){
        this.mot = mot;
    }

    public String getMot(){
        return mot;
    }

    public int getMlangueID() {
        return mlangueID;
    }

    public int getMsemantiqueID() {
        return msemantiqueID;
    }

    public int getMthemeID() {
        return mthemeID;
    }

    @Override
    public String toString(){
        return mot;
    }

}
