package com.example.mymi.interrovoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NouveauMot extends AppCompatActivity {

    ClassBDD bdd;
    ListView mListView;
    ArrayList<StringEtID> langues;
    StringEtID theme;
    int sid;
    List<EditTextLabel> languesField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nouveau_mot);

        bdd = new ClassBDD(this);
        bdd.open();

        mListView = (ListView) findViewById(R.id.listView5);

        Intent intent = getIntent();
        langues = intent.getParcelableArrayListExtra("langues");
        languesField = new ArrayList<>();
        for (StringEtID langue : langues) {
            languesField.add(new EditTextLabel(langue));
        }
        EditTextLabelAdapter adapter = new EditTextLabelAdapter(this, languesField);
        theme = intent.getParcelableExtra("theme");
        sid = bdd.prochaineSemantique();
        mListView.setAdapter(adapter);
        Log.d(Constants.LOG, "[Act] Ouverture Nouveau Mot");
    }

    private void enregistreMot(){
        EditTextLabelAdapter adapter = (EditTextLabelAdapter)mListView.getAdapter();
        ArrayList<StringEtID> champsTextes = adapter.getChampsTextes();
        for (int i = 0; i < champsTextes.size(); i++) {
            if (!champsTextes.get(i).getLabel().equals("")){
                bdd.insertMot(champsTextes.get(i).getLabel(), langues.get(i).getId(), theme.getId(), sid);
                Toast.makeText(this, "Mot ajouté",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void nouveauMotEncore(View view) {
        //enregistrer nouveau mot
        enregistreMot();
        //rafraîchir la page
        EditTextLabelAdapter adapter = new EditTextLabelAdapter(this, languesField);
        mListView.setAdapter(adapter);
        sid = bdd.prochaineSemantique();
    }

    public void nouveauMotFini(View view) {
        //enregistre nouveau mot
        enregistreMot();
        bdd.close();
        //rafraîchir la page
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture Nouveau Mot");
    }
}
