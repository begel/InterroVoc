package com.example.mymi.interrovoc;

/**
 * Created by mymi on 27/03/16.
 */
public class EditTextLabel {
    private StringEtID label;
    private String mot;

    public EditTextLabel(StringEtID label, String mot) {
        this.label = label;
        this.mot = mot;
    }

    public EditTextLabel(StringEtID label) {
        this.label = label;
    }

    public StringEtID getStringEtID() {
        return label;
    }

    public void setStringEtID(StringEtID label) {
        this.label = label;
    }

    public String getMot() {
        return mot;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }
}
