package com.example.mymi.interrovoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Dictionnaire extends AppCompatActivity {
    ClassBDD bdd;
    ArrayList<Mot> traductions;
    HashMap<Integer,String> langues;
    String motATraduire;
    AutoCompleteTextView textView;
    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionnaire);
        Log.d(Constants.LOG, "[Act] Ouverture Dictionnaire");
        bdd = new ClassBDD(this);
        bdd.open();
        ArrayList<Mot> mots = (ArrayList) bdd.getMots();
        langues = bdd.getLanguesMap();
        if (langues.size()==0 || mots==null){
            Toast.makeText(this, "Vérifie que tu as des langues, des thèmes et des mots avant de consulter ton dictionnaire",
                    Toast.LENGTH_LONG).show();
            bdd.close();
            finish();
        }
        textView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        ArrayAdapter<Mot> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mots);
        textView.setAdapter(adapter);
        mListView = (ListView) findViewById(R.id.listView8);
    }

    public void startAccueil(View view){
        bdd.close();
        finish();
        Log.d(Constants.LOG, "[Act] Fermeture Dictionnaire");
    }

    public void dicoTraduire(View view){
        motATraduire = textView.getText().toString();
        traductions = bdd.getMotsTraduits(motATraduire);
        MotAdapter adapter = new MotAdapter(this, traductions,langues);
        mListView.setAdapter(adapter);
        Log.d(Constants.LOG,"[Dico] Traduction de "+motATraduire);
    }
}
